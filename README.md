# wednesday

Its wednesday my dudes!

## Running

Clone this repository somewhere, then make a `secret.yaml` in repo root with the following information about your bot in this format: 

```yaml
bot_id: botID   #this is the "first part" of your bot secret, the string before the first `:`
api_key: apikey #this is the second part of your bot secret, the string after the first `:`
```

You may need to `touch` the `chat_ids.yaml` for it to work the first time. From then, it should be able to manage the subscription data on its own.