import bot
import yaml
import datetime
import time

with open('secret.yaml', 'r') as yamlfile:
	secret = yaml.safe_load(yamlfile)

mybot = bot.bot(secret['bot_id'], secret['api_key'])
gif = open('wednesday.mp4', 'rb')

id_arr = []

id_file = open('chat_ids.yaml', 'r')
for id in id_file:
	id_arr.append(id.strip())

id_file.close()

while(True):

	time.sleep(1) # don't peg the CPU | maybe we're going to miss the 0th second with this | or maybe not

	u = mybot.getUpdates()

	if (u !=0):
		print(u)
		new_id = str(u['result'][0]['message']['chat']['id'])
		msg = u['result'][0]['message']['text']
		if ((msg == '/start') or (msg == '/start@'+secret['bot_name'])) and (new_id not in id_arr):
			mybot.sendMessage(new_id, 'You have subscribed to wednesday bot, my dudes!')
			id_arr.append(new_id)
			id_file = open('chat_ids.yaml', 'a')
			id_file.write(new_id)
			id_file.write('\n')
			id_file.close()

	t = datetime.datetime.now()
	if(t.weekday() == 2):
		if(t.hour == t.minute == t.second == 0):
			for id in id_arr:
				mybot.sendGif(id, gif)